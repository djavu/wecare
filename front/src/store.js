import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        jwt: '',
    },
    mutations: {
        updateToken(state, newToken) {
            Vue.set(state, 'jwt', newToken);
        },

    }
});

export default store;
