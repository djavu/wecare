import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import Amplify, * as AmplifyModules from 'aws-amplify';
import { AmplifyPlugin } from 'aws-amplify-vue';
import aws_exports from './aws-exports';
import VueRouter from 'vue-router'
import routes from './routes';
import store from './store';
import axios from 'axios';

// Make Axios play nice with Django CSRF
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';


Amplify.configure(aws_exports);
Vue.use(AmplifyPlugin, AmplifyModules);
Vue.use(VueRouter)
Vue.config.productionTip = false

const router = new VueRouter({
    mode: 'history',
  routes // short for `routes: routes`
})


new Vue({
    router,
    store,
  render: h => h(App),
}).$mount('#app')
