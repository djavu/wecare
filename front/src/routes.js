const routes = [
    {
        path: '/',
        component: () => import('./components/Base.vue'),
        children: [
            {
                path: '/',
                component: () => import('./views/Home.vue'),
                name: 'home',
            },
            {
                path: '/matching',
                component: () => import('./views/Matching.vue'),
                name: 'matching',
            },
            {
                path: '/scheduling',
                component: () => import('./views/Scheduling.vue'),
                name: 'scheduling',
            },
        ]
    },
    {
        path: '/login',
        component: () => import('./views/Login.vue'),
    },
    {
        path: '/amplify',
        name: 'amplify-login',
        component: () => import('./views/LoginAmplify.vue'),
    },
    {
        path: '/agora',
        component: () => import('./components/AgoraVue/Base.vue'),
        children: [
            {
                path: '/meeting',
                name: 'Meeting',
                component: () => import('./components/AgoraVue/Meeting.vue')
            },
            {
                path: '/',
                name: 'chat',
                component: () => import('./components/AgoraVue/Home.vue')
            },
        ]
    },
];

export default routes;
