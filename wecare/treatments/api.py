from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.permissions import AllowAny

from treatments.models import Treatment
from treatments.serializers import TreatmentSerializer


class TreatmentListAPIView(ListAPIView):
    permission_classes = (AllowAny, )
    serializer_class = TreatmentSerializer
    queryset = Treatment.objects.all()


class TreatmentCreateAPIView(CreateAPIView):
    permission_classes = (AllowAny, )
    serializer_class = TreatmentSerializer
    queryset = Treatment.objects.all()
