from django.db import models


class Treatment(models.Model):
    title = models.CharField(null=True, max_length=500)
    goal = models.PositiveIntegerField()
    progress = models.PositiveIntegerField()
