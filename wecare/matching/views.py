from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from .serializers import TherapistSerializer
from .matching import *
from .models import Therapist


class TherapistRetrieveAPIView(APIView):
    permission_classes = (AllowAny, )

    def post(self, request):
        if 'answers' not in request.data:
            return Response(status=HTTP_400_BAD_REQUEST)
        answers = request.data['answers']
        therapist = get_object_or_404(Therapist.objects.all(), pk=get_therapist(answers)) \
            if answers is not None else Therapist.objects.order_by('?').first()
        serializer = TherapistSerializer(therapist)
        return Response(data=serializer.data)
