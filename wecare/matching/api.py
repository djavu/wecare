from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny

from matching import serializers, models


class QuestionsListAPIView(ListAPIView):
    permission_classes = (AllowAny, )
    serializer_class = serializers.QuestionSerializer
    queryset = models.AssessmentQuestion.objects.all().order_by('index')
