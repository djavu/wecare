from django.test import TestCase

from .matching import *


# Create your tests here.
class ClusterClassificationTestCase(TestCase):

    fixtures = ['matching/fixtures/questions.json']

    def setUp(self):
        generate_features()
        self.nb_answers = 10
        
    def testClustering(self):
        for i in range(self.nb_answers):
            answer = [randint(0, 2) if i != 1 else randint(0, 1) for i in range(14)]
            cluster_index = get_group(answer)
            self.assertTrue(cluster_index in list(range(10)))
