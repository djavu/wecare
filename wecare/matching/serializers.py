from rest_framework import serializers

from .models import AssessmentQuestion, AssessmentAnswer, Therapist


class QuestionSerializer(serializers.ModelSerializer):

    class Meta:
        model = AssessmentQuestion
        fields = ('title', 'index', 'options', )


class AnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = AssessmentAnswer
        fields = ('answer', 'question__index', )


class TherapistSerializer(serializers.ModelSerializer):

    class Meta:
        model = Therapist
        fields = ('name', 'university', 'ranking', 'age')
