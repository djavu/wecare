"""Matching between users and therapists."""

from sklearn.cluster import KMeans
from random import randint
import numpy as np

from matching import models

from .models import Therapist

NB_OF_ANSWERS = int(1000 / 14)  # number of patients


def generate_features():
    """Poblar arreglo de features con respuestas aleatorias."""
    for answer in range(NB_OF_ANSWERS):
        for question in models.AssessmentQuestion.objects.all():
            models.AssessmentAnswer.objects.create(question=question,
                                                   answer=randint(0, len(question.options)-1),
                                                   patient=answer)


def get_features():
    features = []
    for patient in range(NB_OF_ANSWERS):
        answers = [answer.answer for answer in models.AssessmentAnswer.objects.filter(patient=patient).order_by('question__index')]
        features.append(answers)

    return features


def define_space(n_clusters=10):
    """Defines a hyperspace in R-Nfeatures.""" 
    X = get_features()
    Xt = np.transpose(X)
    # km = KMeans(n_clusters=10).fit(np.column_stack((Xt[0], Xt[2])))
    kms = KMeans(n_clusters=n_clusters).fit(X)
    cts = kms.cluster_centers_
    
    return kms
    
    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    # bx = [-0.5, 0.5, 1.5, 2.5]
    # by = [-0.5, 0.5, 1.5, 2.5]
    # h, xe, ye = np.histogram2d(Xt[0], Xt[1], bins=(bx, by))
    # h=h.T
    # xm, ym = np.meshgrid(xe, ye)
    # ax.pcolormesh(xm, ym, h)
    # plt.imshow(h)
    # plt.colorbar()
    
    # plt.savefig('test.png')


def get_group(answer):
    """Given a user's set of answers, returns a cluster group (therapist)."""
    kms = define_space()
    cluster_index = kms.predict(np.array(answer).reshape(1, -1))[0]
    import pdb; pdb.set_trace()
    return cluster_index['array'][0]


def get_therapist(cluster_index):
    """Returns a therapist object, given the cluster index."""
    # TODO proper dispatching
    return Therapist.objects.get(pk=cluster_index+1)
