from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.db.models import PROTECT as PROTECC


class AssessmentQuestion(models.Model):
    title = models.CharField(max_length=500)
    index = models.PositiveIntegerField()
    options = ArrayField(models.CharField(max_length=500))


class AssessmentAnswer(models.Model):
    answer = models.PositiveIntegerField()
    question = models.ForeignKey('matching.AssessmentQuestion', on_delete=PROTECC)

    patient = models.PositiveIntegerField()


class Therapist(models.Model):

    name = models.CharField(max_length=100)
    age = models.PositiveIntegerField()
    university = models.CharField(max_length=100)
    ranking = models.FloatField(null=True)
