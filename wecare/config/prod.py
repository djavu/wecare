from .base import *

STATIC_ROOT = os.path.join(BASE_DIR, 'static_root')
ALLOWED_HOSTS = ['*']
