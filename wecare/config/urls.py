"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from matching.api import QuestionsListAPIView
from treatments.api import TreatmentListAPIView, TreatmentCreateAPIView
from matching.views import TherapistRetrieveAPIView

urlpatterns = [
    path('admin/', admin.site.urls),
]

# REST Auth
urlpatterns += [

    # JWT auth
    path('auth/obtain_token/', obtain_jwt_token),
    path('auth/refresh_token/', refresh_jwt_token),
    path('treatment-list/', TreatmentListAPIView.as_view()),
    path('treatment-create/', TreatmentCreateAPIView.as_view(), name='treatment_create'),
    path('question-list/', QuestionsListAPIView.as_view()),
    path('matching/therapist_retrieve', TherapistRetrieveAPIView.as_view(), name='retrieve_therapist'),
]
